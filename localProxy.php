<?php
	// Skript zur Weiterleitung von Anfragen aus dem Masterportal (POST & GET) an die entsprechenden Datenquellen. 
	// Aus Sicherheitsgründen werden nur in der Whitelist aufgeführte Ziele akzeptiert. Die Whitelist wird dabei aus
	// der service-Definition des Masterportals generiert.
	
	// ini_set('display_errors', 1);
	// ini_set('display_startup_errors', 1);
	// error_reporting(E_ALL);

	// If this request is a preflight there's nothing more to do
	if ($_SERVER["REQUEST_METHOD"]=="OPTIONS") {
		exit();
	}

	// Get the target url and additional parameters if it's a request of type GET.
	$q = $_GET["url"];

	if (!isset($q) || empty($q)) {
		exit();
	}

	// Erzeuge anhand der services-internet.json eine Whitelist für Anfragen
	$servicesConfigPath = "./lgv-config/services-internet.json";
	$servicesConfigJSON = file_get_contents($servicesConfigPath);
	$servicesConfig = json_decode($servicesConfigJSON);

	$whitelist = [];

	foreach($servicesConfig as $service) {

		$scheme = parse_url($service->url, PHP_URL_SCHEME);
		$urlToHost = parse_url($service->url, PHP_URL_HOST);

		if ($scheme != false && $urlToHost != false) {
			$url = $scheme."://".$urlToHost;

			if (!in_array($url, $whitelist)) {
				array_push($whitelist, $url);
			}
		}
	}

	// print_r($whitelist);

	// Extract the target host and check wether it's part of the whitelist
	$qScheme = parse_url($q, PHP_URL_SCHEME);
	$qHost = parse_url($q, PHP_URL_HOST);

	if (!$qScheme) {
		exit();
	}

	if (!$qHost) {
		exit();
	}

	$qCheck = $qScheme."://".$qHost;

	if (!in_array($qCheck, $whitelist)) {
		exit();
	} 

	// Prepare the request to target. If it's a requst of ty�pe POST add the data of the post-boy.
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $q);

	if ($_SERVER["REQUEST_METHOD"]=="POST" && $_SERVER["CONTENT_TYPE"]=="text/xml") {
		$p = file_get_contents('php://input');

		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt($ch, CURLOPT_POSTFIELDS,$p);
		curl_setopt($ch, CURLOPT_POST, 1);
	}

	// Insert CURL proxy here (if necessary)
	// curl_setopt($ch, CURLOPT_PROXY, "<PROXYURL>");

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	// Execute the request and retun the result
	echo curl_exec($ch);
?>
