# mpProxy

Beispiel für ein Proxy zur Verwendung mit dem Masterportal. Der Proxy unterstützt (derzeit) sowohl GET- als auch POST-Requests mit
"Content-Type: text/xml". Die Aufzurufende URL ist dabei in jedem Fall als GET-Parameter in der Form "url=*" zu übergeben, zum Beispiel:

`https://proxy.example.com/proxy.php?url=https%3A%2F%2Fwww.example.com%2Fgeo`

das Argument muss dabei encodiert übergeben werden. Ein vollständiger GET-Request sähe wie folgt aus:

`https://proxy.example.com/proxy.php?url=https%3A%2F%2Fwww.example.com%2Fgeo%3Fservice%3Dwfs%26request%3DGetCapabilities`

Aus Sicherheitsgründen werden nur Anfragen zugelassen, deren Ziel in einer Whitelist enthalten ist. Dabei kann die Whitelist entweder im Code festgeschrieben sein oder dynamsich aus der Service-Definition des Masterportals generiert werden.

Zusätzlich stellt dieses Repository eine .htaccess-Datei zur Verwendung mit rewriteProxy bereit, wodurch der Proxy über ein Rewrite angesprochen werden kann.

Vorraussetzung zur Verwendung der Proxys ist php mit curl. Für curl muss eventuell ein Proxy definiert werden (siehe Kommentar im Code), wenn der ausführende Server keinen direkten Zugriff auf das Internet hat.

## localProxy

Beispiel-Proxy zur Verwendung unter der gleichen Domain wie die, auf der auch das zugehörige Masterportal gehostet wird. Die Whitelist wird dynamisch generiert.

## remoteProxy

Beispiel-Proxy der durch Verwendung von CORS-Headern auch auf einer anderen Domain zur Verfügung gestellt werden darf.
Hier ist die Whitelist im Code festgeschrieben, könnte aber natürlich auch dynamisch generiert werden (solange Zugriff auf die service-Definition besteht).

## rewriteProxy

Dieser Proxy wird über die .htaccess-Datei angesprochen. Zentral ist hier ein Mapping von einem Schlüssel, der für einen Host steht, auf eine zugehörige URL. Durch dieses Mapping ist direkt ein Whitelisting realisiert. Alternativ könnte auch eine Weiterleitung mittels Apache-ProxyPass durchgeführt werden. Da dies aber nur auf Systemen möglich ist bei denen die Apache-Konfiguration beeinflusst werden kann (und ein Proxy nicht über eine .htaccess-Datei gesteuert werden kann) wird hier der Weg über ein PHP-Skript eingeschlagen.
