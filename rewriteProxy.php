<?php
	// Skript zur Weiterleitung von Anfragen aus dem Masterportal (POST & GET) an die entsprechenden Datenquellen. 
	// Aus Sicherheitsgründen werden nur in der Whitelist aufgeführte Ziele akzeptiert. Zur Verwendung mit auf einem
	// Webspace laufenden Instanzen des Masterportals (keine Konfiguration des Webservers möglich, eventuell kein 
	// Internet-Zugang oder PHP) werden CORS-Header gesetzt. Sollte aus diesem Skript nicht direkt auf das Internet zugegriffen
	// werden können besteht weiter unten die möglichkeit noch einen zusätzlichen Proxy zu setzen.
	// Zusätzlich zu diese Skript wurde eine htaccess-Datei so konfiguriert, dass Pfade auf den url-Parameter (get) gemappt werden.

	// ini_set('display_errors', 1);
	// ini_set('display_startup_errors', 1);
	// error_reporting(E_ALL);

	// Set header for cors. Allow origins defined by aray
	$allowOrigin = array("https://localhost:9001");
	$origin = $_SERVER["HTTP_ORIGIN"];

	if ($origin == "") {
		// Try to use referer if origin isn't set
		$parsedReferer = parse_url($_SERVER["HTTP_REFERER"]);
		$origin = $parsedReferer["scheme"]."://".$parsedReferer["host"];
		if (array_key_exists("port", $parsedReferer)) $origin .= ":".$parsedReferer["port"];
    }

	if (in_array($origin, $allowOrigin)) {
		header("Access-Control-Allow-Origin: ".$origin);
	} 
	else {
		// Unknown origin: Exit
		exit();
	}

	header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
	header("Access-Control-Allow-Headers: Content-Type");

	// If this request is a preflight there's nothing more to do
	if ($_SERVER["REQUEST_METHOD"]=="OPTIONS") {
		exit();
	}

	// Get the target url and additional parameters if it's a request of type GET.
	$q = $_GET["url"];

	if (!isset($q) || empty($q)) {
		exit();
	}

	// Get the hostname and check for known url
	$qSlash = stripos($q, "/");
	$qBase = substr($q, 0, $qSlash);

	$name2url = [];

	// Whitelist: Mapping von Servernamen auf Server-URLs
	// $name2url["example_de"] = "https://example.de";

	if (!array_key_exists($qBase, $name2url)) {
		echo "Target not found: ".$qBase;
		exit();
	}

	$qURL = $name2url[$qBase].substr($q, $qSlash); 

	// Attach additional GET-parameters 
	$paramStr = "";
	if (count($_GET)>1) {
		foreach($_GET as $key => $value) {
			if ($key != "url") {
				$paramStr = $paramStr.urlencode($key)."=".urlencode($value)."&";
			}
		}
		$paramStr = substr($paramStr, 0, strlen($paramStr)-1);
		$qURL = $qURL."?".$paramStr;
	}

	// Prepare the request to target. If it's a requst of type POST add the data of the post-boy.
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $qURL);

	if ($_SERVER["REQUEST_METHOD"]=="POST") {

		$contentTypeHeader = "Content-Type: ".$_SERVER["CONTENT_TYPE"];
		curl_setopt($ch, CURLOPT_HTTPHEADER, array($contentTypeHeader));

		$p = file_get_contents('php://input');
		curl_setopt($ch, CURLOPT_POSTFIELDS,$p);

		curl_setopt($ch, CURLOPT_POST, 1);
	}

	// Proxy Settings
	// $http_proxy="<ProxyURL:Port>";
	// curl_setopt($ch, CURLOPT_PROXY, $http_proxy);

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	// Execute the request and retun the result
	$content = curl_exec($ch);

	$contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);
	header("Content-Type: ".$contentType);

	echo $content;
?>
