<?php

	// Skript zur Weiterleitung von Anfragen aus dem Masterportal (POST & GET) an die entsprechenden Datenquellen. 
	// Aus Sicherheitsgründen werden nur in der Whitelist aufgeführte Ziele akzeptiert. Zur Verwendung mit auf einem
	// Webspace laufenden Instanzen des Masterportals (keine Konfiguration des Webservers möglich, eventuell kein 
	// Internet-Zugang oder PHP) werden CORS-Header gesetzt.

	// ini_set('display_errors', 1);
	// ini_set('display_startup_errors', 1);
	// error_reporting(E_ALL);

	// Set header for cors. Origin "*" sollte hier durch die tatsächlichen URL des Masterportals ersetzt werden. 
	header("Access-Control-Allow-Origin: *");
	header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
	header("Access-Control-Allow-Headers: Content-Type");

	// If this request is a preflight there's nothing more to do
	if ($_SERVER["REQUEST_METHOD"]=="OPTIONS") {
		exit();
	}

	// Get the target url and additional parameters if it's a request of type GET.
	$q = $_GET["url"];

	if (!isset($q) || empty($q)) {
		exit();
	}

	// Extract the target host and check wether it's part of the whitelist
	$qScheme = parse_url($q, PHP_URL_SCHEME);
	$qHost = parse_url($q, PHP_URL_HOST);

	if (!$qScheme) {
		exit();
	}

	if (!$qHost) {
		exit();
	}

	$qCheck = $qScheme."://".$qHost;

	$whitelist = [];

	$whitelist[] = "http://191.233.106.244";
	$whitelist[] = "http://extmap.hbt.de";
	$whitelist[] = "http://hmbtg.geronimus.info";
	$whitelist[] = "https://geodienste.hamburg.de";
	$whitelist[] = "https://geoportal-hamburg.de";
	$whitelist[] = "http://geodienste.hamburg.de";
	$whitelist[] = "https://map1.hamburg.de";
	$whitelist[] = "https://sg.geodatenzentrum.de";
	$whitelist[] = "https://v08.viom-system.de";
	$whitelist[] = "http://metaver.de";

	if (!in_array($qCheck, $whitelist)) {
		exit();
	} 

	// Prepare the request to target. If it's a requst of ty�pe POST add the data of the post-boy.
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $q);

	if ($_SERVER["REQUEST_METHOD"]=="POST" && $_SERVER["CONTENT_TYPE"]=="text/xml") {
		$p = file_get_contents('php://input');

		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
		curl_setopt($ch, CURLOPT_POSTFIELDS,$p);
		curl_setopt($ch, CURLOPT_POST, 1);
	}

	// Insert CURL proxy here (if necessary)
	// curl_setopt($ch, CURLOPT_PROXY, "<PROXYURL>");

	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	// Execute the request and retun the result
	echo curl_exec($ch);
?>
